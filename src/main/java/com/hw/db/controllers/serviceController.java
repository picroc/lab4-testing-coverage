package com.hw.db.controllers;

import com.hw.db.DAO.ServiceDAO;
import com.hw.db.models.Message;
import com.hw.db.models.Status;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"}, allowCredentials = "true")
@RequestMapping("/api/service")
public class serviceController {

    @PostMapping(path = "/clear", consumes = "application/json", produces = "application/json")
    public ResponseEntity Clear() {
        ServiceDAO.Clear();
        return ResponseEntity.status(HttpStatus.OK).body(new Message("Очистка базы успешно завершена"));
    }


    @GetMapping(path = "/status")
    public ResponseEntity Status() {
        Status resp = ServiceDAO.Status();
        return ResponseEntity.status(HttpStatus.OK).body(resp);
    }

}
