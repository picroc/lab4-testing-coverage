package com.hw.db.DAO;

import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class threadDAOTests {
    @Test
    void testTreeSort1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);

        new PostDAO.PostMapper();
        new ThreadDAO.ThreadMapper();

        ThreadDAO.treeSort(1, 1, 1, false);
        verify(mockJdbc).query(eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts " +
                "WHERE id = ?)  ORDER BY branch LIMIT ? ;"), any(PostDAO.PostMapper.class), any(Integer.class),
                any(Integer.class), any(Integer.class));
    }

    @Test
    void testTreeSort2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);

        new PostDAO.PostMapper();
        new ThreadDAO.ThreadMapper();

        ThreadDAO.treeSort(1, 1, 1, true);
        verify(mockJdbc).query(eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts " +
                "WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"), any(PostDAO.PostMapper.class), any(Integer.class),
                any(Integer.class), any(Integer.class));
    }
}
