package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

public class userDAOTests {
    @Test
    void testUserChange1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        User user = new User(
                null,
                null,
                null,
                null
        );

        UserDAO.Change(user);
        verifyNoInteractions(mockJdbc);
    }

    @Test
    void testUserChange2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        User user = new User(
                "picroc",
                null,
                null,
                null
        );

        UserDAO.Change(user);
        verifyNoInteractions(mockJdbc);
    }

    @Test
    void testUserChange3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        User user = new User(
                "picroc",
                null,
                null,
                "about"
        );

        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"), (Object) any());
    }

    @Test
    void testUserChange4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        User user = new User(
                "picroc",
                null,
                "full",
                null
        );

        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                (Object) any());
    }

    @Test
    void testUserChange5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        User user = new User(
                "picroc",
                null,
                "full",
                "about"
        );

        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                any(), any(), any());
    }

    @Test
    void testUserChange6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        User user = new User(
                "picroc",
                "somemail@mail.mail",
                null,
                null
        );

        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), (Object) any());
    }

    @Test
    void testUserChange7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        User user = new User(
                "picroc",
                "somemail@mail.mail",
                null,
                "about"
        );

        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                any(), any(), any());
    }

    @Test
    void testUserChange8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        User user = new User(
                "picroc",
                "somemail@mail.mail",
                "full",
                null
        );

        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                any(), any(), any());
    }

    @Test
    void testUserChange9() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);

        User user = new User(
                "picroc",
                "somemail@mail.mail",
                "full",
                "about"
        );

        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE " +
                "nickname=?::CITEXT;"), any(), any(), any(), any());
    }


}
