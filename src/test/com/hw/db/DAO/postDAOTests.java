package com.hw.db.DAO;

import java.sql.Timestamp;

import com.hw.db.models.Post;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class postDAOTests {
    @Test
    void testSetPost1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Timestamp changedAt = new Timestamp(System.currentTimeMillis());

        Post existingPost = new Post(
                "picroc",
                changedAt,
                "forum",
                "mess",
                1,
                1,
                false
        );
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), any(PostDAO.PostMapper.class)
                , eq(1))).thenReturn(existingPost);

        new PostDAO.PostMapper();
        Post changedPost = new Post(
                "picroc",
                changedAt,
                "forum",
                "mess",
                1,
                1,
                false
        );
        PostDAO.setPost(1, changedPost);

        verify(mockJdbc).queryForObject(any(), any(PostDAO.PostMapper.class), any());
        verifyNoMoreInteractions(mockJdbc);
    }

    @Test
    void testSetPost2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Timestamp changedAt = new Timestamp(System.currentTimeMillis());

        Post existingPost = new Post(
                "picroc",
                changedAt,
                "forum",
                "mess",
                1,
                1,
                false
        );
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), any(PostDAO.PostMapper.class)
                , eq(1))).thenReturn(existingPost);

        new PostDAO.PostMapper();
        Post changedPost = new Post(
                "picroc1",
                changedAt,
                "forum",
                "mess",
                1,
                1,
                false
        );
        PostDAO.setPost(1, changedPost);

        verify(mockJdbc).update(eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"), any(Object.class),
                any());
    }

    @Test
    void testSetPost3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Timestamp changedAt = new Timestamp(System.currentTimeMillis());

        Post existingPost = new Post(
                "picroc",
                changedAt,
                "forum",
                "mess",
                1,
                1,
                false
        );
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), any(PostDAO.PostMapper.class)
                , eq(1))).thenReturn(existingPost);

        new PostDAO.PostMapper();
        Post changedPost = new Post(
                "picroc",
                new Timestamp(1),
                "forum",
                "mess",
                1,
                1,
                false
        );
        PostDAO.setPost(1, changedPost);

        verify(mockJdbc).update(eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                any(Object.class), any());
    }

    @Test
    void testSetPost4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Timestamp changedAt = new Timestamp(System.currentTimeMillis());

        Post existingPost = new Post(
                "picroc",
                changedAt,
                "forum",
                "mess",
                1,
                1,
                false
        );
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), any(PostDAO.PostMapper.class)
                , eq(1))).thenReturn(existingPost);

        new PostDAO.PostMapper();
        Post changedPost = new Post(
                "picroc",
                changedAt,
                "forum",
                "mess1",
                1,
                1,
                false
        );
        PostDAO.setPost(1, changedPost);

        verify(mockJdbc).update(eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"), any(Object.class)
                , any());
    }

    @Test
    void testSetPost5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Timestamp changedAt = new Timestamp(System.currentTimeMillis());

        Post existingPost = new Post(
                "picroc",
                changedAt,
                "forum",
                "mess",
                1,
                1,
                false
        );
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), any(PostDAO.PostMapper.class)
                , eq(1))).thenReturn(existingPost);

        new PostDAO.PostMapper();
        Post changedPost = new Post(
                "picroc1",
                new Timestamp(1),
                "forum",
                "mess",
                1,
                1,
                false
        );
        PostDAO.setPost(1, changedPost);

        verify(mockJdbc).update(eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true " +
                "WHERE id=?;"), any(), any(), any());
    }

    @Test
    void testSetPost6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Timestamp changedAt = new Timestamp(System.currentTimeMillis());

        Post existingPost = new Post(
                "picroc",
                changedAt,
                "forum",
                "mess",
                1,
                1,
                false
        );
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), any(PostDAO.PostMapper.class)
                , eq(1))).thenReturn(existingPost);

        new PostDAO.PostMapper();
        Post changedPost = new Post(
                "picroc1",
                changedAt,
                "forum",
                "mess1",
                1,
                1,
                false
        );
        PostDAO.setPost(1, changedPost);

        verify(mockJdbc).update(eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                any(), any(), any());
    }

    @Test
    void testSetPost7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Timestamp changedAt = new Timestamp(System.currentTimeMillis());

        Post existingPost = new Post(
                "picroc",
                changedAt,
                "forum",
                "mess",
                1,
                1,
                false
        );
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), any(PostDAO.PostMapper.class)
                , eq(1))).thenReturn(existingPost);

        new PostDAO.PostMapper();
        Post changedPost = new Post(
                "picroc",
                new Timestamp(1),
                "forum",
                "mess1",
                1,
                1,
                false
        );
        PostDAO.setPost(1, changedPost);

        verify(mockJdbc).update(eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true " +
                "WHERE id=?;"), any(), any(), any());
    }

    @Test
    void testSetPost8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);

        Timestamp changedAt = new Timestamp(System.currentTimeMillis());

        Post existingPost = new Post(
                "picroc",
                changedAt,
                "forum",
                "mess",
                1,
                1,
                false
        );
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), any(PostDAO.PostMapper.class)
                , eq(1))).thenReturn(existingPost);

        new PostDAO.PostMapper();
        Post changedPost = new Post(
                "picroc1",
                new Timestamp(1),
                "forum",
                "mess1",
                1,
                1,
                false
        );
        PostDAO.setPost(1, changedPost);

        verify(mockJdbc).update(eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , " +
                "isEdited=true WHERE id=?;"), any(), any(), any(), any());
    }
}
