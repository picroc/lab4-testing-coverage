package com.hw.db.DAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class forumDAOTests {
    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"),
                Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #2")
    void ThreadListTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", null, "12.02.2019", false);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)" +
                "::TIMESTAMP WITH TIME ZONE ORDER BY created;"), Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #3")
    void ThreadListTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", null, "12.02.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)" +
                "::TIMESTAMP WITH TIME ZONE ORDER BY created desc;"), Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #4")
    void ThreadListTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", 10, "12.02.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)" +
                "::TIMESTAMP WITH TIME ZONE ORDER BY created desc LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #5")
    void ThreadListTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", 10, "12.02.2019", false);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)" +
                "::TIMESTAMP WITH TIME ZONE ORDER BY created LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #6")
    void ThreadListTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", 10, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created desc " +
                "LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    void testUserList1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext ORDER BY nickname;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", null, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext ORDER BY nickname desc;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", null, "12.02.2019", false);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext AND  nickname > (?)::citext ORDER BY nickname;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", null, "12.02.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext AND  nickname < (?)::citext ORDER BY nickname desc;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", 10, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext ORDER BY nickname LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", 10, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext ORDER BY nickname desc LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", 10, "12.09.2019", null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void testUserList8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", 10, "12.09.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"), Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }
}
